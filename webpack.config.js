const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

const isDevelopment = process.env.NODE_ENV === 'development'

module.exports = {
	entry: './client/js/App.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name]-bundle.js'
	},
	watch: isDevelopment,
	watchOptions: {
    ignored: '/node_modules/'
  },
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: '/node_modules/',
				loaders: [
					'babel-loader',
					'eslint-loader'
				],
			},
			{
				test: /\.(scss|sass|css)$/,
				exclude: '/node_modules/',
				loaders: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: isDevelopment,
							importLoaders: true,
						}
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: isDevelopment
						}
					}
				],
			}
		]
	},
	plugins: [
		new WebpackShellPlugin({
			onBuildStart: ['echo "Starting"'],
			onBuildEnd: ['node -r @babel/register ./server/server.js']
		}),
		new BrowserSyncPlugin({
      host: 'localhost',
      port: 3100,
      proxy: 'http://localhost:3000/'
		}),
		new StylelintPlugin(),
		new MiniCssExtractPlugin(),
	],
	node: {
		fs: 'empty'
	},
	resolve: {
		extensions: ['*', '.js', '.scss']
	},
};
