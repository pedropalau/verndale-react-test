const getValueFor = (option, getValueFn, defaultValue = '') => {
  if (!option) {
    return defaultValue;
  }
  return getValueFn(option);
};

export default getValueFor;
