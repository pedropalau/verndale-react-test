const getNextOption = (options, key, getValueFn, down = false) => {
  if (!options || options.length === 0) {
    return '';
  }

  const index = options.findIndex((o) => {
    const v = getValueFn(o);
    return v === key;
  });

  if (index === -1) {
    return getValueFn(options[0]);
  }

  let nextIndex = -1;
  if (down === false) {
    nextIndex = index + 1;
    if (nextIndex >= options.length) {
      nextIndex = 0;
    }
  } else {
    nextIndex = index - 1;
    if (nextIndex < 0) {
      nextIndex = options.length - 1;
    }
  }

  if (typeof options[nextIndex] === 'undefined') {
    return '';
  }

  return getValueFn(options[nextIndex]);
};

export default getNextOption;
