const checkIfCharIsPrintable = (c) => c.length === 1 && c.match(/\S/);

export default checkIfCharIsPrintable;
