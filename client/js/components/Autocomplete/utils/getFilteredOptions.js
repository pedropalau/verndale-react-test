import checkFilterString from './checkFilterString';

const getFilteredOptions = ({
  options: optionsToFilter,
  filter: f,
  getValueForOption,
  getLabelForOption,
}) => {
  if (!optionsToFilter || optionsToFilter.length === 0) {
    return [];
  }

  if (!f) {
    return optionsToFilter;
  }

  const filter = f.toLowerCase();
  const filteredOptions = optionsToFilter.filter((option) => {
    const value = getValueForOption(option);
    if (!value) {
      return false;
    }

    let isValidByFilter = checkFilterString(value, filter);
    if (!isValidByFilter) {
      const label = getLabelForOption(option);
      isValidByFilter = checkFilterString(label, filter);
    }

    return isValidByFilter;
  });

  return filteredOptions;
};

export default getFilteredOptions;
