const checkFilterString = (value, filter) => value.toLowerCase().indexOf(filter) !== -1;

export default checkFilterString;
