import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const defaultProps = {
  selected: false,
  highlight: false,
  onClick: () => {},
  onMouseEnter: () => {},
  onMouseLeave: () => {},
};

const propTypes = {
  htmlId: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  highlight: PropTypes.bool,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  children: PropTypes.node.isRequired,
};

const Item = ({
  htmlId,
  selected,
  highlight,
  onClick,
  onMouseEnter,
  onMouseLeave,
  children,
}) => (
  <li
    id={htmlId}
    className={classnames(
      'autocomplete--item',
      highlight && 'autocomplete--item-hover',
      selected && 'autocomplete--item-active',
    )}
    aria-selected={selected}
    role="option"
  >
    <button
      className="autocomplete--item-handler"
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      type="button"
    >
      {children}
    </button>
  </li>
);

Item.defaultProps = defaultProps;
Item.propTypes = propTypes;

export default Item;
