import React from 'react';
import PropTypes from 'prop-types';

import phrases from './phrases';

const ClearIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    className="autocomplete-icon"
    fill="currentColor"
  >
    <path fillRule="evenodd" d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z" />
    <path fillRule="evenodd" d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z" />
  </svg>
);

const defaultProps = {
  ariaLabel: phrases.clear,
  children: <ClearIcon />,
  onClick: () => {},
};

const propTypes = {
  ariaLabel: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

const Clear = ({
  ariaLabel,
  children,
  onClick,
}) => (
  <button
    className="autocomplete--search-clear"
    aria-label={ariaLabel}
    onClick={onClick}
    type="button"
  >
    {children}
  </button>
);

Clear.defaultProps = defaultProps;
Clear.propTypes = propTypes;

export default Clear;
