import React, { useState, useEffect, forwardRef } from 'react';
import PropTypes from 'prop-types';

const defaultProps = {
  value: '',
  isExpanded: false,
  placeholder: '',
  onClick: () => {},
  onBlur: () => {},
  onFocus: () => {},
  onChange: () => {},
  onKeyDown: () => {},
  onKeyUp: () => {},
};

const propTypes = {
  value: PropTypes.string,
  isExpanded: PropTypes.bool,
  ownsHtmlId: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onChange: PropTypes.func,
  onKeyDown: PropTypes.func,
  onKeyUp: PropTypes.func,
};

const Input = forwardRef(({
  value: defaultValue,
  isExpanded,
  ownsHtmlId,
  placeholder,
  onClick,
  onBlur,
  onFocus,
  onChange,
  onKeyDown,
  onKeyUp,
}, ref) => {
  const [currentValue, setCurrentValue] = useState(defaultValue);

  useEffect(() => {
    setCurrentValue(defaultValue);
  }, [defaultValue]);

  return (
    <input
      type="text"
      className="autocomplete--search-input"
      placeholder={placeholder}
      value={currentValue}
      role="combobox"
      autoComplete="off"
      onClick={onClick}
      onBlur={onBlur}
      onFocus={onFocus}
      onChange={onChange}
      onKeyDown={onKeyDown}
      onKeyUp={onKeyUp}
      aria-autocomplete="both"
      aria-controls="search"
      aria-expanded={isExpanded}
      aria-haspopup="true"
      aria-owns={ownsHtmlId}
      ref={ref}
    />
  );
});

Input.defaultProps = defaultProps;
Input.propTypes = propTypes;

export default Input;
