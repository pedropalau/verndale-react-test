import React, {
  useState,
  useCallback,
  useEffect,
  useRef,
} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Clear from './Clear';
import Input from './Input';
import Item from './Item';
import Open from './Open';
import Suggestions from './Suggestions';
import Spin from './Spin';

import defaultGetLabelForOption from './utils/getLabelForOption';
import defaultGetValueForOption from './utils/getValueForOption';
import getFilteredOptions from './utils/getFilteredOptions';
import getNextOption from './utils/getNextOption';
import getOptionFor from './utils/getOptionFor';
import getValueFor from './utils/getValueFor';
import checkIfCharIsPrintable from './utils/checkIfCharIsPrintable';

import keycodes from './keycodes';
import phrases from './phrases';

import './autocomplete.scss';

const defaultProps = {
  options: [],
  isLoading: false,
  placeholder: phrases.placeholder,
  // A toggler feature is hard to implement
  showToggler: false,
  // This is an intent to get focus/blur events working properly
  strictMode: false,
  getLabelForOption: defaultGetLabelForOption,
  getValueForOption: defaultGetValueForOption,
  onChange: () => {},
  onSelect: () => {},
};

const propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ),
  isLoading: PropTypes.bool,
  placeholder: PropTypes.string,
  showToggler: PropTypes.bool,
  strictMode: PropTypes.bool,
  getLabelForOption: PropTypes.func,
  getValueForOption: PropTypes.func,
  onChange: PropTypes.func,
  onSelect: PropTypes.func,
};

const Autocomplete = ({
  options: initialOptions,
  isLoading,
  placeholder,
  showToggler,
  strictMode,
  getLabelForOption,
  getValueForOption,
  onChange,
  onSelect,
}) => {
  const [currentOption, setValue] = useState(null);
  const [isOpen, setOpen] = useState(false);
  const [isInputFocus, setInputFocus] = useState(false);
  const [options, setOptions] = useState([]);
  const [selected, setSelectedItem] = useState('');
  const [highlighted, setHighlightedIndex] = useState('');
  const [filter, setFilter] = useState('');
  const inputRef = useRef();

  const handleChangeCallback = useCallback((e) => {
    onChange(e.currentTarget.value);
  }, [
    filter,
    onChange,
  ]);

  const closeSuggestions = useCallback(() => {
    setOpen(false);
    setInputFocus(false);
  }, [isOpen]);

  const openSuggestions = useCallback(() => {
    setOpen(true);
    setInputFocus(true);
  }, []);

  useEffect(() => {
    const filteredOptions = getFilteredOptions({
      options: initialOptions,
      getLabelForOption,
      getValueForOption,
      filter,
    });
    setOptions(filteredOptions);
    if (filteredOptions.length === 0) {
      closeSuggestions();
    } else if (filter && !isOpen) {
      openSuggestions();
    }
  }, [
    isOpen,
    initialOptions,
    filter,
    currentOption,
  ]);

  const toggleSuggestions = useCallback(() => {
    inputRef.current.focus();
  }, []);

  const handleBlurCallback = useCallback(() => {
    setInputFocus(false);
    if (strictMode) {
      setTimeout(closeSuggestions, 300);
    }
  }, [strictMode]);

  const handleClickCallback = useCallback(() => {
    if (strictMode) {
      openSuggestions();
    }
  }, []);

  const handleFocusCallback = useCallback((open) => {
    setInputFocus(true);
    if (strictMode) {
      if (open === false) {
        openSuggestions();
      } else {
        closeSuggestions();
      }
    }
  }, []);

  const goDown = useCallback((o, s) => {
    const i = getNextOption(o, s, getValueForOption);
    setSelectedItem(i);
  }, []);

  const goUp = useCallback((o, s) => {
    const i = getNextOption(o, s, getValueForOption, true);
    setSelectedItem(i);
  }, []);

  const doEsc = useCallback(() => {
    setHighlightedIndex('');
    setSelectedItem('');
    setValue(null);
    onSelect(null);
    closeSuggestions();
  }, []);

  const setItem = useCallback((value, o, open) => {
    if (value !== '') {
      const selectedOption = getOptionFor({
        options: o,
        selected: value,
        getValueForOption,
      });
      setValue(selectedOption);
      onSelect(selectedOption);
    } else {
      setValue(null);
      onSelect(null);
    }
    toggleSuggestions(open);
  }, []);

  const handleKeydownCallback = useCallback((e) => {
    switch (e.keyCode) {
      case keycodes.DOWN:
        goDown(options, selected);
        break;

      case keycodes.UP:
        goUp(options, selected);
        break;

      case keycodes.RETURN:
        if (filter !== '') {
          setItem(selected, options, isOpen);
        }
        break;

      case keycodes.ESC:
        doEsc();
        break;

      default:
        break;
    }
  }, [
    options,
    selected,
    isOpen,
    filter,
  ]);

  const handleKeyupCallback = useCallback((e) => {
    const {
      key,
      keyCode,
      currentTarget: {
        value: currentValue,
      },
    } = e;

    let stopEvent = false;

    if (checkIfCharIsPrintable(key)) {
      setFilter(filter + key);
    }

    if (keyCode === keycodes.ESC) {
      return;
    }

    switch (keyCode) {
      case keycodes.RETURN:
        if (isOpen) {
          closeSuggestions();
        }
        setFilter('');
        break;

      case keycodes.BACKSPACE:
        if (currentValue.length > 0) {
          const valueToUpdate = currentValue.slice(0, -1);
          setFilter(valueToUpdate);
          if (valueToUpdate === '') {
            closeSuggestions();
          }
        }
        setValue(null);
        onSelect(null);
        setHighlightedIndex('');
        setSelectedItem('');
        stopEvent = true;
        break;

      case keycodes.LEFT:
      case keycodes.RIGHT:
      case keycodes.HOME:
      case keycodes.END:
        setFilter(currentValue);
        break;

      default:
        stopEvent = checkIfCharIsPrintable(key);
        break;
    }

    if (stopEvent) {
      e.stopPropagation();
      e.preventDefault();
    }
  }, [
    filter,
    initialOptions,
    isOpen,
  ]);

  const onItemClick = useCallback((option, items) => {
    setFilter('');
    const value = getValueForOption(option);
    setHighlightedIndex(value);
    setSelectedItem(value);
    setItem(value, items, true);
    closeSuggestions();
  }, []);

  const onItemMouseEnter = useCallback((option) => {
    const v = getValueForOption(option);
    if (selected !== v) {
      setHighlightedIndex(v);
    }
  }, [selected]);

  const onItemMouseLeave = useCallback(() => {
    setHighlightedIndex('');
  }, [selected]);

  const removeFilter = useCallback(() => {
    setFilter('');
    setSelectedItem('');
    setHighlightedIndex('');
    setValue(null);
    onSelect(null);
    inputRef.current.focus();
    if (!strictMode) {
      closeSuggestions();
    }
  }, [inputRef]);

  const isFiltering = filter !== '' || currentOption !== null;

  return (
    <div
      className={classnames(
        'autocomplete',
        isOpen && 'autocomplete-isopen',
        isInputFocus && 'autocomplete-isfocus',
      )}
      tabIndex="-1"
    >
      <div className="autocomplete--search">
        <Input
          value={getValueFor(currentOption, getLabelForOption, filter)}
          placeholder={placeholder}
          ownsHtmlId="suggestions"
          isExpanded={isOpen}
          onClick={handleClickCallback}
          onBlur={handleBlurCallback}
          onFocus={() => handleFocusCallback(isOpen)}
          onChange={handleChangeCallback}
          onKeyDown={handleKeydownCallback}
          onKeyUp={handleKeyupCallback}
          ref={inputRef}
        />
        <div className="autocomplete--search-actions">
          {isLoading && <Spin />}
          {isFiltering && <Clear onClick={removeFilter} />}
          {(!isLoading && showToggler) && <Open onClick={toggleSuggestions} />}
        </div>
      </div>
      {isOpen && options.length > 0 && (
        <Suggestions
          htmlId="suggestions"
          isOpen
        >
          {
            options.map((option) => {
              const value = getValueForOption(option);
              return (
                <Item
                  key={`autocomplete${value}`}
                  htmlId={value}
                  selected={value === selected}
                  highlight={value === highlighted}
                  onClick={(e) => onItemClick(option, options, e)}
                  onMouseEnter={() => onItemMouseEnter(option)}
                  onMouseLeave={() => onItemMouseLeave(option)}
                >
                  {getLabelForOption(option)}
                </Item>
              );
            })
          }
        </Suggestions>
      )}
    </div>
  );
};

Autocomplete.defaultProps = defaultProps;
Autocomplete.propTypes = propTypes;

export default Autocomplete;
