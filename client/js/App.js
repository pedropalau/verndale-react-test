import React, { useState, useCallback } from 'react';
import ReactDOM from 'react-dom';

import Autocomplete from './components/Autocomplete';
import StateInfo from './components/StateInfo';

import '../sass/app.scss';

const App = () => {
  const [isLoading, setLoading] = useState(false);
  const [state, setSelectedState] = useState(null);
  const [foundStates, setFoundStates] = useState({
    count: 0,
    data: [],
  });

  const onAutocompleteChangeFilter = useCallback((term) => {
    setLoading(true);
    fetch(`/api/states?term=${term}`)
      .then((response) => {
        response.json()
          .then(({ count, data }) => {
            setFoundStates({
              count,
              data,
            });
            setLoading(false);
          });
      });
  }, []);

  return (
    <div className="flex flex-col items-center justify-center">
      <div className="flex flex-col items-center justify-center mb-6">
        <h1 className="text-2xl font-medium mb-2">
          Autocomplete Component
        </h1>
        <h2 className="text-lg font-normal text-gray-600">
          This is an US States autocomplete component built using the latest React features.
        </h2>
      </div>
      <Autocomplete
        placeholder="Search state"
        options={foundStates.data}
        isLoading={isLoading}
        getLabelForOption={({ name }) => name}
        getValueForOption={({ abbreviation }) => abbreviation}
        onSelect={setSelectedState}
        onChange={onAutocompleteChangeFilter}
      />
      <div className="container mx-auto">
        <div
          className="flex justify-start w-full mt-4 max-w-screen-md mx-auto"
          style={{
            minHeight: '100px',
          }}
        >
          <StateInfo state={state} />
        </div>
        <footer className="mt-4">
          <p className="text-sm text-gray-600 text-center leading-normal mb-2">
            This is a demo project implemented to apply to a job opportunity.
            &nbsp;
            I do not recommend using it in a production application since it
            needs more review, tests and extra features that are not implemented
            in this version. To see more information related to this project
            please visit the
            &nbsp;
            <a href="https://gitlab.com/pepalau/verndale-react-test" target="_blank" rel="noopener noreferrer">repository</a>
            .
          </p>
          <p className="text-sm flex items-center justify-center">
            <a href="https://twitter.com/palauisaac" target="_blank" rel="noopener noreferrer">
              Twitter: @palauisaac
            </a>
            &nbsp;
            &nbsp;
            <a href="https://github.com/peterpalau" target="_blank" rel="noopener noreferrer">
              GitHub: @palauisaac
            </a>
            &nbsp;
            &nbsp;
            <a href="https://gitlab.com/pepalau" target="_blank" rel="noopener noreferrer">
              GitLab: @palauisaac
            </a>
          </p>
        </footer>
      </div>
    </div>
  );
};

ReactDOM.render(
  <App />,
  document.getElementById('app'),
);
